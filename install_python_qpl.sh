#!/bin/bash  

# Modified version of https://github.com/fonnesbeck/ScipySuperpack for QPL.
# I have removed some redundant packages, disabled the xcode check because of the slow WiFi
# Added packages like qutip, and removed disabled the option so it just installs python 2.

# set -euvx

cd "$(mktemp -d -t install_python_qpl)"

readonly BREW_PATH="/usr/local/bin"
readonly BREW="${BREW_PATH}/brew"
readonly GIT="${BREW_PATH}/git"

if ! [[ -x "${BREW}" ]]; then
    if [[ "${BREW_PATH}" == /usr/local/bin ]]; then
        # Installing Homebrew...
        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    else
        echo 'You are using a custom homebrew location and I could not find the brew binary.'
        exit 1
    fi
fi

#if ! [[ -x /usr/bin/clang ]]; then
    # No clang detected; Installing XCode Command Line Tools...
#    xcode-select --install
#fi

export PATH="${BREW_PATH}:${PATH}"

# Ensure Homebrew formulae are updated
"${BREW}" update

if ! [[ -x "${GIT}" ]]; then
    "${BREW}" install git
fi

# Add science tap
"${BREW}" tap homebrew/science

# Python tools and utilities

"${BREW}" install python
readonly PYTHON="${BREW_PATH}/python"
readonly EASY_INSTALL="${BREW_PATH}/easy_install"
readonly PIP="${BREW_PATH}/pip"

#Install gcc and some python stuff.
"${BREW}" install gcc
"${PIP}" install -U nose
"${PIP}" install -U six
"${PIP}" install -U patsy
"${PIP}" install -U pygments
"${PIP}" install -U sphinx
"${PIP}" install -U cython

# IPython
"${BREW}" install zmq
"${PIP}" install  pyzmq
"${PIP}" install ipython[notebook]

# Numpy
"${PIP}" install numpy

# SciPy
"${BREW}" install gfortran
"${PIP}" install scipy

# Matplotlib
"${BREW}" install freetype
"${BREW}" install libpng
"${PIP}" install matplotlib

# Rest of the things
"${PIP}" install pymc
"${PIP}" install qutip

echo "export PATH=/usr/local/bin:\$PATH" >> ~/.bash_profile
